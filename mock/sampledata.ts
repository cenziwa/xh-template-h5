export default {
  'GET /api/Infra/UpdateLog': (req, res) => {
    setTimeout(() => {
      res.send({ success: true, data: '"## 6.0.24\r\n`2024-05-15`\r\n- [新增]应用布局设计新增通用组件\r\n- [新增]表格列配置新增表头样式配置\r\n- [优化]审核人/检验人条件更变为，hisid+姓名，如果没有hisid则为logid+姓名\r\n- [修复]完全匹配时，无效标本数不正确的问题\r\n## 6.0.23\r\n`2024-04-29`\r\n- [新增]统计维度，采集人/检验人/审核人\r\n## 6.0.22\r\n`2024-04-28`\r\n- [新增]TAT明细列 检验人，审核人，申请-打印，打印-采集，采集-送达\r\n- [修复]危急值明细报source不能为null的问题\r\n- [修复]按科别维度统计时，不合格，召回报错的问题\r\n## 6.0.21\r\n`2024-04-26`\r\n- [新增]维度 日（月次），日（年次），患者科别\r\n- [重要]修复Xinghe.Utility底层bug（6.0.20弃用）\r\n## 6.0.20\r\n`2024-04-22`\r\n- [修复]不合格统计按诊疗项目维度统计未归类的问题\r\n- [新增]危急值分析模块，增加分析项目的统计维度和分析条件\r\n- [新增]召回报告数明细新增检验单号\r\n- [修复]TAT明细,送出时间送达时间不正确的问题\r\n## 6.0.19\r\n`2024-04-10`\r\n- [修复]危急值分析报错的问题\r\n- [修复]危急值不及时数统计不正确的问题\r\n- [修复]TAT不及时数统计数据和明细数据不一致的问题\r\n- [优化]不合格原因分类字典改为从质量指标视图中取\r\n- [优化]抗凝字典改为从质量指标视图中取\r\n- [新增]TAT环节 送出-接收 送出-最后接收\r\n## 6.0.18\r\n`2024-03-21`\r\n- [修复]召回分析中样本数排除召回报告\r\n- [新增]深圳儿童第三方调用\r\n## 6.0.17\r\n`2024-03-18`\r\n- [修复]按实验室分组统计时,显示实验室ID而不是名称的bug\r\n## 6.0.16\r\n`2024-03-06`\r\n*需更新clickhouse脚本,依赖S18 6.1.7 *\r\n- [新增]危急值环节新增产生-电话报告,感知-电话报告,电话报告-反馈\r\n- [新增]条件/维度分组 新增检查等级\r\n- [新增]危急值明细新增病人姓名、性别、年龄、报告人员、感知人员\r\n## 6.0.15\r\n`2024-02-28`\r\n- [修复]TAT分析项目/诊疗项目归类统计结果展示不正确的bug\r\n## 6.0.14\r\n`2024-01-26`\r\n- [新增]不合格标本总数(已选归类)指标\r\n- [修复]归类小计不正确的bug\r\n- [修复]分组切换后点击明细空白的bug\r\n## 6.0.13\r\n`2024-01-26`\r\n- [修复]归类方案相关的一些问题\r\n## 6.0.12\r\n`2024-01-25`\r\n- [新增]归类方案\r\n- [修复]周次计算从周日-周六 修改为 从周一到周日\r\n- [新增]不合格明细新增一批字段\r\n- [修复]明细项重复的问题\r\n- [修复]按诊疗项目分组看明细,部分项目空白的问题\r\n- [新增]允许点击小计值查看明细\r\n- [修复]同机构,同科室,同模块,同归类类型下 归类名称不能重复的问题\r\n- [修复]危急值报告率计算错误问题\r\n- [修复]危急值明细新增一批字段\r\n## 6.0.11\r\n`2023-11-27`\r\n- [修正]不合格分析明细不显示诊疗项目\r\n- [新增]召回报告分析\r\n## 6.0.10\r\n`2023-11-16`\r\n- [新增]TAT明细新增诊疗项目\r\n- [新增]维度,条件新增诊疗项目归类\r\n- [新增]条件新增人员筛选\r\n- ## 6.0.9\r\n`2023-11-08`\r\n- [新增]维度新增周次\r\n- [修复]电报率超越100%的问题\r\n## 6.0.8\r\n`2023-11-08`\r\n- [新增]默认时间范围新增上一周\r\n- [修复]不合格危急值按专业组维度统计报错的问题\r\n## 6.0.7\r\n`2023-11-07`\r\n- [修复]时间段归类查询条件报错\r\n- [修复]时间段归类作为维度时报错\r\n- [优化]时间段归类设置\r\n## 6.0.6\r\n`2023-11-07`\r\n- [修复]由于业务系统写入变更,不合格分类字典由名称修正为ID\r\n- [修复]危急值总样本数数据不准确\r\n## 6.0.5\r\n`2023-11-01`\r\n- [新增]不合格,危急值新增明细\r\n- [修复]电报率不准确的问题\r\n## 6.0.4\r\n`2023-11-01`\r\n- [优化]危急值查询优化\r\n- [新增]新增采集院区/申请院区 维度条件\r\n## 6.0.3\r\n`2023-10-23`\r\n- [优化]禁止机构账号登录\r\n- [修复]服务报错问题\r\n## 6.0.2\r\n`2023-10-23`\r\n- [优化]不合格分析优化\r\n- [新增]新增危急值分析\r\n- [修复]其他问题修复\r\n## 6.0.1\r\n`2023-08-16`\r\n- TAT部分发布测试\r\n- 表结构调整\r\n- 新增TAT查看明细功能\r\n## 6.0.0\r\n`2023-08-16`\r\n- SQLSugar改造\r\n\r\n' });
    }, 2000);
  },
  '/api/Infra/HealthCheck': (req, res) => {
    res.send({ success: true, data: '6.0.0' });
  },
  '/api/Infra/GetWarterMarkOption': (req, res) => {
    res.send({
      success: true, data: {
        "OpenWaterMark": true,
        "WaterMarkFiledPattern": "{USER_NAME}_{USER_NO}",
        "font": {
          "color": "#dadad8",
          "fontSize": 15,
          "fontWeight": "normal",
          "fontStyle": "normal",
          "fontFamily": null
        },
        "WaterMarkProps": {
          "rotate": -32,
          "gap": [
            100,
            100
          ],
          "content": "杏和软件_1461"
        }
      }
    });
  },
  '/api/Sample/GetError': (req, res) => {

    res.send({
      success: false, msg: '发生了内部异常', exception: `2024-04-27 02:00:26.252 +08:00 [ERR] Redis connection error SocketFailure
StackExchange.Redis.RedisConnectionException: SocketFailure (ReadSocketError/TimedOut, last-recv: 359) on 114.55.127.200:6379/Interactive, Idle/Faulted, last: INFO, origin: ReadFromPipe, outstanding: 1, last-read: 79s ago, last-write: 19s ago, unanswered-write: 19s ago, keep-alive: 60s, state: ConnectedEstablished, mgr: 62 of 64 available, in: 0, in-pipe: 0, out-pipe: 0, last-heartbeat: 0s ago, last-mbeat: 0s ago, global: 0s ago, v: 2.6.122.38350
 ---> System.Net.Sockets.SocketException (10060): 由于连接方在一段时间后没有正确答复或连接的主机没有反应，连接尝试失败。
   at Pipelines.Sockets.Unofficial.Internal.Throw.Socket(Int32 errorCode) in /_/src/Pipelines.Sockets.Unofficial/Internal/Throw.cs:line 59
   at Pipelines.Sockets.Unofficial.SocketAwaitableEventArgs.GetResult() in /_/src/Pipelines.Sockets.Unofficial/SocketAwaitableEventArgs.cs:line 74
   at Pipelines.Sockets.Unofficial.SocketConnection.DoReceiveAsync() in /_/src/Pipelines.Sockets.Unofficial/SocketConnection.Receive.cs:line 64
   at System.IO.Pipelines.Pipe.GetReadResult(ReadResult& result)
   at System.IO.Pipelines.Pipe.GetReadAsyncResult()
   at StackExchange.Redis.PhysicalConnection.ReadFromPipe() in /_/src/StackExchange.Redis/PhysicalConnection.cs:line 1711
   --- End of inner exception stack trace ---` });

  },
}





