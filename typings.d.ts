import 'umi/typings';


declare global {
  interface Window extends Global {
    cefJs: any
  }
}


declare module "dayjs" {
  interface Dayjs {
    toCommonDateFormat(): string;
    toCommonTimeFormat(): string;
    toDayOfWeekExt(): string
  }
}

