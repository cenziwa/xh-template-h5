/**
 * @Author: Harity
 * @Date: 2024-05-28 11:16:57
 * @LastEditTime: 2024-06-12 09:52:39
 * @LastEditors: Harity
 * @Description: 
 * @FilePath: /xh-template-H5/.eslintrc.js
 * @可以输入预定的版权声明、个性签名、空行等
 */
module.exports = {
  extends: require.resolve('@umijs/max/eslint'),
  rules: {
    '@typescript-eslint/no-unused-vars': 'warn',
    'react-hooks/rules-of-hooks': 'error', // 检查 Hook 的规则
    'react-hooks/exhaustive-deps': 'warn', // 检查 effect 的依赖
  },
}
