/*
 * @Author: Harity
 * @Date: 2024-05-28 11:16:57
 * @LastEditTime: 2024-05-29 13:39:06
 * @LastEditors: Harity
 * @Description: 
 * @FilePath: /xh-template-H5/config/routes.ts
 * 可以输入预定的版权声明、个性签名、空行等
 */
export default [
  {
    path: '/',
    routes: [
      {
        path: '/',
      },
      {
        path: '/uni_signin',
        title: '统一登录页',
      },
      {
        path: '/redirect_url',
        title: '统一登录回调',
      },
      {
        path: '/',
        // component: '@/layouts',
        routes: [
          {
            path: '/home',
            component: '@/pages/Dashboard',
            title: '看板',
          },
        ],
      },

    ]
  }
]