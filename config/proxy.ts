export default {
  '/api': {
    target: 'https://114.55.38.185:18446',
    //target: 'https://localhost:18446',
    changeOrigin: true,
    secure: false,
    pathRewrite: { '^': '' },
  },
}
