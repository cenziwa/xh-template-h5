/*
 * @Author: Harity
 * @Date: 2024-05-28 11:16:57
 * @LastEditTime: 2024-05-29 13:38:05
 * @LastEditors: Harity
 * @Description: 
 * @FilePath: /xh-template-H5/config/config.ts
 * 可以输入预定的版权声明、个性签名、空行等
 */

import { defineConfig } from "umi";
import appSettings from './appSettings'
import routes from './routes'
import proxy from './proxy'
export default defineConfig({
  hash: true,
  favicons: ['/static/favicon.png'],
  https: {
    //禁用http2 解决浏览器偶尔报错的问题(开发模式下)
    http2: false,
  },
  title: appSettings.title,
  targets: {
    chrome: 80,
  },

  jsMinifier: 'terser',

  //以下两项需兼容低版本时开启
  // legacy: {},
  // autoprefixer: {
  //   overrideBrowserslist: [
  //     "Android 4.1",
  //     "iOS 7.1",
  //     "Chrome > 31",
  //     "ie >= 8"
  //   ]
  // },
  mfsu: false,
  //注意联调时关闭mock选项
  mock: {},
  routes: routes,
  proxy: proxy,
  npmClient: 'yarn',
  headScripts: [
    `/static/iconfont/iconfont.js`,
  ],
});
