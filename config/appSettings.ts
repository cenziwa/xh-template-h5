
export interface IAppSettings {
  title: string
  /**
   * 开发调试时iconfont地址,注:发布时次地址不生效,请注意更新/public/static/iconfont/iconfont.js
   */
  iconfontUrl: string
  moduleId: string
  /**
   * token续签地址
   */
  renewTokenUrl: string

}
const setting: IAppSettings = {
  title: '起步框架',
  iconfontUrl: '//at.alicdn.com/t/c/font_4555995_mj3k2tj5qon.js',
  moduleId: 'H07',
  renewTokenUrl: '/api/Account/ReNewToken'
}


export default setting

