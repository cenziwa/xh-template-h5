/*
 * @Author: Harity
 * @Date: 2024-05-28 11:16:57
 * @LastEditTime: 2024-05-29 13:40:00
 * @LastEditors: Harity
 * @Description:
 * @FilePath: /xh-template-H5/src/pages/Dashboard/index.tsx
 * 可以输入预定的版权声明、个性签名、空行等
 */
import React, { useState, useEffect } from 'react'
import { Badge, TabBar } from 'antd-mobile'
import { AppOutline, MessageOutline, MessageFill, UnorderedListOutline, UserOutline } from 'antd-mobile-icons'
import styles from './styles.less'

export default function Dashboard() {


  const tabs = [
    {
      key: 'home',
      title: '首页',
      icon: <AppOutline />,
      badge: Badge.dot,
    },
    {
      key: 'todo',
      title: '待办',
      icon: <UnorderedListOutline />,
      badge: '5',
    },
    {
      key: 'message',
      title: '消息',
      icon: (active: boolean) => (active ? <MessageFill /> : <MessageOutline />),
      badge: '99+',
    },
    {
      key: 'personalCenter',
      title: '我的',
      icon: <UserOutline />,
    },
  ]

  const [activeKey, setActiveKey] = useState('todo')

  const onChange = (e) => {
    setActiveKey(e)
  }
  return (
    <div className={styles.wrapper}>
      <div className={styles.cont}>{tabs.find((v) => v.key === activeKey)?.title}</div>
      <div className={styles.footer}>
        <TabBar onChange={onChange}>
          {tabs.map((item) => (
            <TabBar.Item key={item.key} icon={item.icon} title={item.title} />
          ))}
        </TabBar>
      </div>
    </div>
  )
}
