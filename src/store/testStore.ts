import { proxy, snapshot, subscribe } from "valtio";
import { useProxy, devtools } from "valtio/utils";


const state = proxy<any>({
  t1: { t2: { t3: '123' } }
});



devtools(state, { name: 'test-state', enabled: true })
const useTestlStore = () => useProxy(state)



export { useTestlStore }