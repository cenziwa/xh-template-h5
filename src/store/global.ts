import { proxy, snapshot, subscribe } from "valtio";
import { useProxy, devtools } from "valtio/utils";

const local = sessionStorage.getItem("global-state");
//由于持久化机制的存在，直接手动修改以下状态会导致状态依然从storage中读取而不生效
//编码时如果需要修改以下状态值，请手动从sessionStorage中清空global-state或重新登录
const state = proxy<IGlobalState>(local ? JSON.parse(local) : {
  isXingTong: true,
  userlocked: false,
  breadcrumb: "",
  currUser: undefined,
  autolockScreenInterval: 6 * 60 * 1000,//默认5分钟
  currHospital: {
    HOSPITAL_ID: '33A001',
    HOSPITAL_CNAME: "浙江省人民医院"
  },
  data: []
});


// 初始化函数
function initialize() {
  // 在这里执行你想要的初始化操作
  state.isXingTong = !!window.cefJs;//true为壳子 false 为浏览器
}


initialize();

devtools(state, { name: 'global-state', enabled: true })
const useGlobalStore = () => useProxy(state)

//注意：鉴于性能问题，建议只针对关键信息做持久化
subscribe(state, () => {
  sessionStorage.setItem("global-state", JSON.stringify(snapshot(state)));
});



export { useGlobalStore }