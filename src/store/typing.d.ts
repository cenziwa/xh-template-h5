
declare interface IGlobalState {
  breadcrumb?: React.ReactNode
  currUser?: SYS6_USER
  currHospital?: SYS6_HOSPITAL_INFO
  currVersion?: string
  /**
   * 是否是杏通环境
   */
  isXingTong: boolean
  userlocked?: boolean
  /**
   * 自动锁屏间隔时间(ms)
   */
  autolockScreenInterval?: number


}
