import { history } from "umi";

const onLogoff = () => {
  //清理session
  sessionStorage.clear();
  history.push('/')
}



export default {
  onLogoff
}