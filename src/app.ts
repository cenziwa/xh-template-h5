/*
 * @Author: Harity
 * @Date: 2024-05-28 11:16:57
 * @LastEditTime: 2024-05-29 13:39:15
 * @LastEditors: Harity
 * @Description: 
 * @FilePath: /xh-template-H5/src/app.ts
 * 可以输入预定的版权声明、个性签名、空行等
 */
import dayjs from 'dayjs'
import appSettings from './../config/appSettings'
// 定义插件
//@ts-ignore
const customPlugin = (option, dayjsClass, dayjsFactory) => {
  const week: any = {
    1: '星期一',
    2: '星期二',
    3: '星期三',
    4: '星期四',
    5: '星期五',
    6: '星期六',
    0: '星期日',
  }
  // 添加自定义方法
  dayjsClass.prototype.toCommonTimeFormat = function () {

    return this.format('YYYY-MM-DD HH:mm:ss');
  };
  dayjsClass.prototype.toCommonDateFormat = function () {

    return this.format('YYYY-MM-DD');
  };
  dayjsClass.prototype.toDayOfWeekExt = function () {
    return week[this.day()].toString()
  }
};
dayjs.extend(customPlugin)

declare module "dayjs" {
  interface Dayjs {
    toCommonDateFormat(): string;
    toCommonTimeFormat(): string;
    toDayOfWeekExt(): string
  }
}

export function onRouteChange({ location, routes, action }) {
  const route = Object.values(routes).find((r) =>
    {
      const rName = r.path.split('/')[1]
      const lName = location.pathname.split('/')[1]
      return rName === lName
    }
  )
  if (route) {
    //这个route.name是根据你的router.js里面标题怎么起的
    document.title = route.title || appSettings.title
  }
}
