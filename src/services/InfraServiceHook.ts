import XRequset from '@/utils/XRequest'
import { useRequest } from 'ahooks'
import React from 'react'


/**
 * 基础设施服务
 */
export default function useInfraService() {
  const option = {
    manual: true
  }

  return {
    /**
     * 获取更新日志
     */
    GetUpdateLog: useRequest(() => {
      return XRequset<string>({
        url: '/api/Infra/UpdateLog',
        method: 'GET',
      })
    }, option),
    /**
     * 获取版本信息
     */
    GetVersion: useRequest(() => {
      return XRequset<string>({
        url: '/api/Infra/HealthCheck',
        method: 'GET',
      })
    }, option),
    /**
     * 获取水印设置
     */
    GetWatermarkOption: useRequest(() => {
      return XRequset<any>({
        url: '/api/Infra/GetWarterMarkOption',
        method: 'GET',
      })
    }, option),
    /**
     * 获取统一登录地址
     */
    GetUnitLoginUrl: useRequest(() => {
      return XRequset<string>({
        url: '/api/Infra/GetUnitLoginUrl',
        method: 'GET',
      })
    }, option),
    /**
   * 获取特定模块地址
   */
    GetModuleUrl: useRequest((params: { moduleId: string }) => {
      return XRequset<string>({
        url: '/api/Infra/GetModuleUrl',
        method: 'GET',
        params
      })
    }, option),

  }
}
