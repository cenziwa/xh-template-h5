import XRequset from '@/utils/XRequest'
import { useRequest } from 'ahooks'
import React from 'react'


/**
 * 样例服务
 */
export default function useSampleService() {
  const option = {
    manual: true
  }

  return {
    /**
     * 获取更新日志
     */
    GetError: useRequest(() => {
      return XRequset<string>({
        url: '/api/Sample/GetError',
        method: 'GET',
      })
    }, option),


  }
}
