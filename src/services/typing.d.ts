declare interface SYS6_USER {
  USER_NO: string
  LOGID: string
  USERNAME: string
  POWER: string
  DEPT_CODE: string
  STATE_FLAG: string
  REMARK: string
  LAB_ID: string
  PHONE_NO: string
  E_MAIL: string
  USER_TYPE: string
  HOSPITAL_ID: string
  USER_CLASS: string
  HIS_ID: string
}
declare interface SYS6_HOSPITAL_INFO {
  HOSPITAL_ID: string
  HOSPITAL_CNAME: string

}

