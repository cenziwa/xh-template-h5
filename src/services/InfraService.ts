/*
 * @Author: Harity
 * @Date: 2024-05-28 11:16:57
 * @LastEditTime: 2024-05-29 09:55:58
 * @LastEditors: Harity
 * @Description: 
 * @FilePath: /xh-template-H5/src/services/InfraServiceHook.ts
 * 可以输入预定的版权声明、个性签名、空行等
 */
import XReqest from '@/utils/XRequest'


/**
 * 基础设施服务
 */


/**
 * 获取更新日志
 */
export async function GetUpdateLog() {
  return XReqest<string>({
    url: '/api/Infra/UpdateLog',
    method: 'GET',
  })
};


/**
 * 获取版本信息
 */
export async function GetVersion() {
  return XReqest<string>({
    url: '/api/Infra/HealthCheck',
    method: 'GET',
  })
}
/**
 * 获取水印设置
 */
export async function GetWatermarkOption() {
  return XReqest<any>({
    url: '/api/Infra/GetWarterMarkOption',
    method: 'GET',
  })
}
/**
 * 获取统一登录地址
 */
export async function GetUnitLoginUrl() {
  return XReqest<string>({
    url: '/api/Infra/GetUnitLoginUrl',
    method: 'GET',
  })
}
/**
* 获取特定模块地址
*/
export async function GetModuleUrl(params: { moduleId: string }) {
  return XReqest<string>({
    url: '/api/Infra/GetModuleUrl',
    method: 'GET',
    params
  })
}

