import XRequset from '@/utils/XRequest'
import { useRequest } from 'ahooks'
import React from 'react'


/**
 * 账户相关服务
 */
export default function useAccountService() {
  const option = {
    manual: true
  }

  return {
    /**
     * 根据token换取用户信息
     */
    GetUserInfoByToken: useRequest(() => {
      return XRequset<SYS6_USER>({
        url: '/api/Account/GetUserInfoByTokenSample',
        method: 'GET',
      })
    }, option),
    /**
     * 续签token
     */
    ReNewToken: useRequest((params: { expriedToken: string; refreshToken: string }) => {
      return XRequset<SYS6_USER>({
        url: '/api/Account/ReNewToken',
        method: 'GET',
        params
      })
    }, option),

  }
}