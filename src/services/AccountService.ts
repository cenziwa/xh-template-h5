/*
 * @Author: Harity
 * @Date: 2024-05-28 11:16:57
 * @LastEditTime: 2024-05-29 09:56:47
 * @LastEditors: Harity
 * @Description: 
 * @FilePath: /xh-template-H5/src/services/AccountServiceHook.ts
 * 可以输入预定的版权声明、个性签名、空行等
 */
import XReqest from '@/utils/XRequest'



/**
   * 根据token换取用户信息
   */
export async function GetUserInfoByToken() {
  return XReqest<SYS6_USER>({
    url: '/api/Account/GetUserInfoByTokenSample',
    method: 'GET',
  })
}
/**
  * 续签token
  */
export async function ReNewToken(params: { expriedToken: string; refreshToken: string }) {
  return XReqest<SYS6_USER>({
    url: '/api/Account/ReNewToken',
    method: 'GET',
    params
  })
};
