/*
 * @Author: Harity
 * @Date: 2024-05-28 11:16:57
 * @LastEditTime: 2024-05-29 13:40:10
 * @LastEditors: Harity
 * @Description: 
 * @FilePath: /xh-template-H5/src/services/index.ts
 * 可以输入预定的版权声明、个性签名、空行等
 */
export * as infraService from './InfraService'
export * as accountService from './AccountService'
