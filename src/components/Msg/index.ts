/*
 * @Author: Harity
 * @Date: 2024-05-29 09:26:50
 * @LastEditTime: 2024-05-29 09:50:57
 * @LastEditors: Harity
 * @Description:
 * @FilePath: /xh-template-H5/src/components/Msg/index.ts
 * 可以输入预定的版权声明、个性签名、空行等
 */
// src/components/Notifications.js
import React from 'react'
import { toast, ToastContainer,Slide } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import './index.less'

const showMsg = (type = 'default', message, options = {}) => {
  const newOptions = {
    position: 'top-center',
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    transition: Slide,
    ...options,
  }

  switch (type) {
    case 'success':
      toast.success(message, newOptions)
      break
    case 'error':
      toast.error(message, newOptions)
      break
    case 'info':
      toast.info(message, newOptions)
      break
    case 'warn':
      toast.warn(message, newOptions)
      break
    default:
      toast(message, newOptions)
  }
}

const Msg = {
  error: (content: React.ReactNode, options?: ToastContainerProps) => showMsg('error', content, options),
  info: (content: React.ReactNode, options?: ToastContainerProps) => showMsg('info', content, options),
  success: (content: React.ReactNode, options?: ToastContainerProps) => showMsg('success', content, options),
  warn: (content: React.ReactNode, options?: ToastContainerProps) => showMsg('warn', content, options),
  loading: (content: string = 'Loading...', key: string = 'loading') => message.loading({ content, key }),
  loadingComplete: (content: string = '完成!', key: string = 'loading') =>
    message.success({ content, key, duration: 2 }),
}

export { Msg,ToastContainer }
