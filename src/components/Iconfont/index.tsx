/*
 * @Author: Harity
 * @Date: 2024-05-28 11:16:57
 * @LastEditTime: 2024-05-29 13:39:34
 * @LastEditors: Harity
 * @Description:
 * @FilePath: /xh-template-H5/src/components/Iconfont/index.tsx
 * 可以输入预定的版权声明、个性签名、空行等
 */
import React from 'react'
import './index.less'

interface propsType {
  type: string
  style?: object
}
const Iconfont: FC<propsType> = ({ type, style }) => {
  return (
    <svg className="icon" style={style} aria-hidden="true">
      <use xlinkHref={`#${type}`}></use>
    </svg>
  )
}
export default Iconfont
