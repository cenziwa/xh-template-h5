import React, { useEffect, useState } from 'react'
import styles from './styles.less'
import Markdown from 'react-markdown'
import rehypeRaw from 'rehype-raw'
import { XPageLoading } from 'xinherc'
import { useInfraService } from '@/services'

export default function Index() {
  const [data, setdata] = useState<string>('')
  const infraService = useInfraService()
  useEffect(() => {
    infraService.GetUpdateLog.runAsync().then((res) => {
      setdata(res)
    })
  }, [])

  return (
    <div className={styles.wrapper}>
      {infraService.GetUpdateLog.loading ? (
        <XPageLoading textColor="#01162c" style={{ background: '#f2f2f2' }} text="加载中..."></XPageLoading>
      ) : (
        <Markdown rehypePlugins={[rehypeRaw]}>{data}</Markdown>
      )}
    </div>
  )
}
