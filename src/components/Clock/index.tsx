/*
 * @Author: chenzenghua
 * @Date: 2023-01-04 17:33:23
 * @LastEditTime: 2024-05-29 13:39:26
 * @LastEditors: Harity
 * @Description:
 */
import { CalendarOutline } from 'antd-mobile-icons'
import { useInterval } from 'ahooks'
import dayjs from 'dayjs'
import React, { useState } from 'react'

interface IProps {
  style?: React.CSSProperties
  dateRange?: any
  showWeek?: boolean
}

export default function Index(props: IProps) {
  const [currTime, setcurrTime] = useState(dayjs())
  useInterval(() => {
    setcurrTime(dayjs())
  }, 1000)
  return (
    <div style={props.style}>
      <CalendarOutlined style={{ marginRight: '5px' }} />
      {currTime.toCommonDateFormat()}
      {props.showWeek && <span style={{ marginLeft: '5px' }}>{currTime.toDayOfWeekExt()}</span>}
    </div>
  )
}
